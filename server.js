const express = require('express');
const https = require('https');
const fs = require('fs');
const path = require('path');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const config = JSON.parse(fs.readFileSync(path.join(
  __dirname,
  'config.json'
)));

var twitchToken;
var twitchStreamerID;

function twitchGetToken() {
  var options = {
    hostname: "id.twitch.tv",
    port: 443,
    path: `/oauth2/token?client_id=${config.twitch.client_id}&client_secret=${config.twitch.client_secret}&grant_type=client_credentials`,
    method: 'POST'
  }
  var req = https.request(options, (res) => {
    res.setEncoding('utf8');
    var nd = "";
    res.on('data', d => {
      nd+=d;
    });
    res.on('end', () => {
      if(!res.complete) {
        console.error("Incomplete...");
      }
      nd = JSON.parse(nd);
      twitchToken = nd.access_token;
    });
  });
  req.on('error', error => {
    console.error(error)
  })
  req.end();
}
function twitchGetID() {
  var options = {
    hostname: "api.twitch.tv",
    port: 443,
    path: `/kraken/users?login=${config.twitch.streamer}`,
    method: 'GET',
    headers: {
      "Client-ID": config.twitch.client_id,
      "Accept": "application/vnd.twitchtv.v5+json"
    }
  }
  var req = https.request(options, (res) => {
    res.setEncoding('utf8');
    var nd = "";
    res.on('data', d => {
      nd+=d;
    });
    res.on('end', () => {
      if(!res.complete) {
        console.error("Incomplete...");
      }
      nd = JSON.parse(nd);
      twitchStreamerID = nd.users[0]._id;
    });
  });
  req.on('error', error => {
    console.error(error)
  });
  req.end();
}
function eventsubGetSubscriptions(callback) {
  var options = {
    hostname: "api.twitch.tv",
    port: 443,
    path: `/helix/eventsub/subscriptions`,
    method: 'GET',
    headers: {
      "Authorization": `Bearer ${twitchToken}`,
      "Client-ID": config.twitch.client_id
    }
  }
  var req = https.request(options, (res) => {
    res.setEncoding('utf8');
    var nd = "";
    res.on('data', d => {
      nd+=d;
    });
    res.on('end', () => {
      if(!res.complete) {
        console.error("Incomplete...");
      }
      nd = JSON.parse(nd);
      callback(nd);
    });
  });
  req.on('error', error => {
    console.error(error)
  });
  req.end();
}
function eventsubDeleteSubscription(id) {
  var options = {
    hostname: "api.twitch.tv",
    port: 443,
    path: `/helix/eventsub/subscriptions?id=${id}`,
    method: 'DELETE',
    headers: {
      "Authorization": `Bearer ${twitchToken}`,
      "Client-ID": config.twitch.client_id
    }
  }
  var req = https.request(options, (res) => {
    console.log(res);
    res.on('end', () => {
      if(!res.complete) {
        console.error("Incomplete...");
      }
    });
  });
  req.on('error', error => {
    console.error(error)
  });
  req.end();
}

twitchGetToken();
var idWait = setTimeout(twitchGetID, 1500);

app.get('/', function(req, res) {
  var options = {
    root: path.join(__dirname, 'dashboard')
  }
  res.sendFile('/index.html', options);
});
http.listen(config.port, () => {
  console.log(`CazOS listening on port ${config.port}`);
});
io.on("connection", socket => {
  console.log("Dashboard connected");
  socket.on('disconnect', () => {
    console.log("Dashboard disconnected");
  });
  socket.on('subRefresh', (msg) => {
    console.log("Got subRefresh")
    eventsubGetSubscriptions((data) => {
      socket.emit('subRefresh', data);
    });
  });
  socket.on('subDelete', (id) => {
    eventsubDeleteSubscription(id);
  });
});